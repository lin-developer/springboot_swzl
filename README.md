# springboot_swzl

#### 介绍
springboot整合ssm的失物招领管理系统，前端门户bootstrap。后端Springboot+ssm。该系统有前端门户和后台管理，唯一的超级管理员是admin。目前网上无同款，项目是自己独立完成的，兼容手机和PC端。

# Ssm_Lost and Found

#### 介绍
基于ssm的校园失物招领管理系统，前端用bootstrap框架，后端用Spring+SpringMVC+Mybatis框架。本系统分为用户操作系统和管理员操作系统，唯一的超级管理员是admin。
部分界面有用js检测鼠标离开输入框即使用ajax传数据会后台实时校对数据是否正确，上传信息也是用ajax，并且有图片预览功能,不需要刷新界面使填写的信息丢失。站点简介界面调用了百度地图api，可以在页面的小地图查看站点位置和备注信息。预约领取招领信息有校对当前日期，不可预约以前的时间去领去领取实物。

项目运行效果图：
![输入图片说明](https://images.gitee.com/uploads/images/2022/0720/150208_78161e49_11309749.jpeg "登录.JPG")
![输入图片说明](https://images.gitee.com/uploads/images/2022/0720/150232_7c7d2a48_11309749.jpeg "个人中心.JPG")
![输入图片说明](https://images.gitee.com/uploads/images/2022/0720/150326_717fb68a_11309749.jpeg "首页.JPG")
![输入图片说明](https://images.gitee.com/uploads/images/2022/0720/150404_9aa3def6_11309749.jpeg "招领.JPG")
![输入图片说明](https://images.gitee.com/uploads/images/2022/0720/150422_89180bdd_11309749.jpeg "寻物.JPG")
![输入图片说明](https://images.gitee.com/uploads/images/2022/0720/150442_6cf68fa6_11309749.jpeg "招领上传.JPG")
![输入图片说明](https://images.gitee.com/uploads/images/2022/0720/150256_6df6983f_11309749.jpeg "管理员.JPG")

项目功能设计，按用户群体划分：
管理员用户：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0720/150546_0bff9b60_11309749.jpeg "管理员流程图.JPG")
未登录用户：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0720/150603_b6af5542_11309749.jpeg "未知用户流程图.JPG")
登录账号用户：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0720/150621_eafbe1c2_11309749.jpeg "用户流程图.JPG")

#### 软件架构
软件架构说明
B/S架构，MVC开发模式

#### 安装教程

1.  下载后直接在idea打开，记得是打开，不要用导入。
2.  sql文件需要自己导入。
3.  系统本身需要有mysql，Spring和Mybatis是已经整合在一起的。

#### 使用说明

1.  如果工具类报错，删了或注释掉，有一两个工具类放在那里只是可能会用到，不一定要使用。
2.  如果静态资源被拦截，把config下的配置类注释掉，在yml配置文件启动里面的配置。后面自己再设置拦截器，基本就没问题了。
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
