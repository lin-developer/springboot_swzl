<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/static/images/swzl.ico">

    <title>后台公告详情</title>
    <link href="/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="/static/indexcss/carousel.css" rel="stylesheet">
    <script src="/static/js/jquery.min.js"></script>
    <script>
        $(function () {
            $("#logout").click(function () {
                logOut();
            });
        })
        function logOut() {
            $.ajax({
                type: "get",
                url: "/user/logout",
                success: function (data) {
                   if (data.code == 20051) {
                    // console.log(data.code);
                    location.reload();
                } else {
                    alert(data.msg);
                }
                },
            });
        }
    </script>
</head>

<body background="../../static/images/background.jpg" style=" background-repeat:no-repeat ;background-size:100% 100%;
background-attachment: fixed;">
<div class="navbar-wrapper">
    <div class="container">
        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">切换导航</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/pages/admin/houTaiIndex.jsp">校园失物招领管理系统后台管理</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/pages/admin/houTaiIndex.jsp">物品管理</a></li>
                        <li><a href="/pages/admin/houTaiLiuYan.jsp">留言管理</a></li>
                        <li><a href="/pages/admin/houTaiZhanDian.jsp">站点管理</a></li>
                        <li class="active"><a href="/pages/admin/houTaiGongGao.jsp">公告管理</a></li>
                        <c:choose>
                            <c:when test="${loginUser!=null and loginUser.authority=='超级管理员'}">
                                <li><a href="/pages/admin/houTaiUser.jsp">用户权限管理</a></li>
                            </c:when>
                            <c:otherwise>

                            </c:otherwise>
                        </c:choose>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <c:choose>
                            <c:when test="${loginUser!=null and loginUser.username.length()>0}">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-haspopup="true" aria-expanded="false" id="username">你好，${loginUser.username}
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a><span id="userid">用户id：${loginUser.id}</span></a></li>
                                        <li><a id="logout">注销登录</a></li>
                                    </ul>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li><a href="/pages/login.jsp">你好，请登录！</a></li>
                            </c:otherwise>
                        </c:choose>

                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container" style="padding:0 28px;margin-top: 100px;">
    <div class="row" style="background: #ffffff;border-radius: 5px;">
        <div class="content-wrapper__router">
            <ul class="breadcrumb">
                <li><a href="/pages/admin/houTaiGongGao.jsp" style="color:#5A5A5A;">公告信息管理</a></li>
                <li class="active"><a style="color:#5A5A5A;">详情</a></li>
            </ul>
        </div>
        <div class="row featurette">
            <div style="margin-left:20px;margin-right:20px;">
                <h2 style="text-align: center">${dbNotice.g_title}</h2>
                <p style="text-align: center">发布日期：
                    <fmt:formatDate value="${dbNotice.g_addTime}" pattern="yyyy-MM-dd"/>
                </p>
                <p class="lead"style="text-align: left;text-indent:2em;">${dbNotice.g_content}</p>
                <p class="lead" style="text-align: right">发布人：${dbNotice.g_username}</p>
            </div>
        </div>
    </div>

</div>
</div>

<!-- FOOTER -->
<div class="container marketing">
    <br>
    <br>
    <footer>
        <p class="pull-right"><a href="#">返回顶部</a></p>
        <p>&copy; 2022 伍中林</p>
    </footer>
</div>
<!-- /.container -->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- 放置在文档末尾，以便页面加载更快 -->
<script src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/static/js/jquery.min.js"><\/script>')</script>
<script src="/static/js/bootstrap.min.js"></script>

</body>
</html>
