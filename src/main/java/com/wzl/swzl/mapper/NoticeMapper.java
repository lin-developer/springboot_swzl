package com.wzl.swzl.mapper;

import com.wzl.swzl.entity.Notice;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface NoticeMapper {
    List<Notice> findNotice(@Param("startCount") int startCount, @Param("endCount") int endCount);

    Notice findNoticeById(String g_id);

    List<Notice> findAllNotice();

    int deleteGGById(String id);

    int addGG(Notice notice);
}
