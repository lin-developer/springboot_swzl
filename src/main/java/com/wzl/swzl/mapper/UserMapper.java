package com.wzl.swzl.mapper;


import com.wzl.swzl.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author lscl
 * @version 1.0
 * @intro:
 */

public interface UserMapper {
    User findByUsername(String username);

    int add(User user);

    User findByUserId(Integer userid);

    //全部普通用户
    List<User> findAllUser();

    //全部管理员
    List<User> findAllGLUser();

    //修改权限为管理员
    int updateGL(String id);

    //修改权限为普通用户，即取消管理员权限
    int updateQX(String id);

    //超级管理员删除用户
    int delete(String id);
}
