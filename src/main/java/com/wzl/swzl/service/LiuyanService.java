package com.wzl.swzl.service;


import com.wzl.swzl.entity.Liuyan;
import com.wzl.swzl.mapper.LiuyanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LiuyanService {
    @Autowired
    private LiuyanMapper liuyanMapper;

    //上传留言
    public boolean add(Liuyan liuyan) {
        return liuyanMapper.add(liuyan) > 0;
    }

    //获取留言表信息
    public List<Liuyan> findLiuYanList(int startCount, int endCount) {
        return liuyanMapper.findLiuYanList(startCount, endCount);
    }

    public Integer findLiuYanTotal() {
        return liuyanMapper.findLiuYanTotal();
    }

    //根据用户id获取留言列表
    public List<Liuyan> findLYByUserId(String userid) {
        return liuyanMapper.findLYByUserId(userid);
    }

    //根据id删除留言列表
    public boolean deleteLYById(String id) {
        return liuyanMapper.deleteLYById(id) > 0;
    }

    //获取全部留言
    public List<Liuyan> findAllLY() {
        return liuyanMapper.findAllLY();
    }

}
