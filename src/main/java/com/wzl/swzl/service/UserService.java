package com.wzl.swzl.service;


import com.wzl.swzl.entity.User;
import com.wzl.swzl.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lscl
 * @version 1.0
 * @intro:
 */
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    /**
     * 根据用户名查询用户
     *
     * @param username
     * @return
     */
    public User findByUsername(String username) {
        return userMapper.findByUsername(username);
    }

    public User findByUserId(Integer userid) {
        return userMapper.findByUserId(userid);
    }

    /**
     * 用户新增
     *
     * @param user
     */
    public boolean add(User user) {
        return userMapper.add(user) > 0;
    }

    public List<User> findAllUser() {
        return userMapper.findAllUser();
    }

    //全部管理员
    public List<User> findAllGLUser() {
        return userMapper.findAllGLUser();
    }

    public boolean updateGL(String id) {
        return userMapper.updateGL(id) > 0;
    }

    //修改权限为普通用户，即取消管理员权限
    public boolean updateQX(String id) {
        return userMapper.updateQX(id) > 0;
    }

    //超级管理员删除用户
    public boolean delete(String id) {
        return userMapper.delete(id) > 0;
    }
}
