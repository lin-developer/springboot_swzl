package com.wzl.swzl.service;


import com.wzl.swzl.entity.Notice;
import com.wzl.swzl.mapper.NoticeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoticeService {
    @Autowired
    private NoticeMapper noticeMapper;

    public List<Notice> findNotice(int startCount, int endCount) {
        return noticeMapper.findNotice(startCount, endCount);
    }

    public Notice findNoticeById(String g_id) {
        return noticeMapper.findNoticeById(g_id);
    }

    public List<Notice> findAllNotice() {
        return noticeMapper.findAllNotice();
    }

    public boolean deleteGGById(String id) {
        return noticeMapper.deleteGGById(id) > 0;
    }

    public boolean addGG(Notice notice) {
        return noticeMapper.addGG(notice) > 0;
    }
}
