package com.wzl.swzl.controller;

import com.wzl.swzl.controller.Result.Code;
import com.wzl.swzl.controller.Result.Msg;
import com.wzl.swzl.controller.Result.Result;
import com.wzl.swzl.entity.Liuyan;
import com.wzl.swzl.entity.Page;
import com.wzl.swzl.service.LiuyanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/liuyan")
public class LiuyanController {

    @Autowired
    private LiuyanService liuyanService;

    //上传留言
    @RequestMapping("/information")
    @ResponseBody
    public Result liuYanAdd(@RequestBody Liuyan liuyan) {
        String liuYanContent = liuyan.getLiuyanneirong().replace(" ", "");
    /*    if (liuYanContent == null || liuYanContent.length() == 0) {
            return new Result(Code.POST_ERR,Msg.ADD_ERR);
        }*/
        Date addTime = new Date();
        liuyan.setLiuyanriqi(addTime);
        liuyan.setAddtime(addTime);
        boolean flag = liuyanService.add(liuyan);
        Integer code = flag ? Code.POST_OK : Code.POST_ERR;
        String msg = flag ? Msg.ADD_OK : Msg.ADD_ERR;
        return new Result(code,msg);
    }


    //获取留言列表
    @GetMapping("/list/{currPage}")
    @ResponseBody
    public Result liuYanList(@PathVariable int currPage) {
        //当前页数
        int pageCount = currPage;
        //总记录数
        Integer totalCount = liuyanService.findLiuYanTotal();
        //重试次数
        int count = 2;
        while (totalCount == 0 && count > 0) {
            totalCount = liuyanService.findLiuYanTotal();
            count--;
        }
        //总页数
        Integer totalPage = totalCount % 10 == 0 ? totalCount / 10 : totalCount / 10 + 1;
        //如果记录数为0或请求页面大于总页数或为负数
        if (pageCount > totalPage || pageCount < 0 || totalCount == 0) {
            return new Result(Code.GET_ERR, Msg.QUERY_ERR);
        } else {//如果数据正常则写成json格式返回前端
            //每页显示数,10条
            Integer pageSize = 10;
            // 计算前索引
            Integer startIndex = (pageCount - 1) * pageSize;
            List<Liuyan> liuYanEntitys = liuyanService.findLiuYanList(startIndex, pageSize);
            //重试次数
            int retry = 2;
            while (liuYanEntitys.size() == 0 && retry > 0) {
                liuYanEntitys = liuyanService.findLiuYanList(startIndex, pageSize);
                retry--;
            }
            if (liuYanEntitys.size() > 0) {
                Map<String,Object> map = new HashMap<>();
                map.put("status","true");
                map.put("totalPage",totalPage);
                map.put("totalCount",totalCount);
                map.put("currPage",pageCount);
                map.put("data",liuYanEntitys);
                return new Result(Code.GET_OK,map,Msg.QUERY_OK);
            } else {
                return new Result(Code.GET_ERR, Msg.QUERY_ERR);
            }
        }
    }


    //根据用户id获取留言列表
    @RequestMapping("/userLY")
    @ResponseBody
    public Result userLY(@RequestBody Liuyan liuyan) {

        Integer user_Id = liuyan.getUserid();
        String userId = String.valueOf(user_Id);
        userId = userId.replace(" ", "");
        if ("".equals(userId) || userId.length() == 0) {
            return new Result(Code.POST_ERR, Msg.QUERY_ERR);
        } else {//如果数据正常则写成json格式返回前端
            List<Liuyan> liuYanEntitys = liuyanService.findLYByUserId(userId);
            if (liuYanEntitys.size() > 0) {
                Map<String,Object> map = new HashMap<>();
                map.put("status","true");
                map.put("data",liuYanEntitys);
                return new Result(Code.POST_OK,map,Msg.QUERY_OK);
            } else {
                return new Result(Code.POST_ERR, Msg.QUERY_ERR);
            }
        }
    }

    //个人根据id删除留言
    @GetMapping("/LYDelete")
    public String LYDelete(String id) {
        if ("".equals(id) || id.length() == 0) {
            return "false";
        }
        liuyanService.deleteLYById(id);
        return "userLiuYan";
    }

    //后台获取留言列表
    @RequestMapping("/HTLY")
    @ResponseBody
    public Result HTLY() {
        List<Liuyan> liuYanEntitys = liuyanService.findAllLY();
        if (liuYanEntitys.size() > 0) {
            Map<String,Object> map = new HashMap<>();
            map.put("status","true");
            map.put("data",liuYanEntitys);
            return new Result(Code.GET_OK,map,Msg.QUERY_OK);
        } else {
            return new Result(Code.GET_ERR, Msg.QUERY_ERR);
        }

    }

    //后台根据id删除留言
    @GetMapping("/HTLYDelete")
    public String HTLYDelete(String id) {
        if ("".equals(id) || id.length() == 0) {
            return "false";
        }
        boolean flag = liuyanService.deleteLYById(id);
        return "admin/houTaiLiuYan";
    }
}
