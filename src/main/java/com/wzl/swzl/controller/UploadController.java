package com.wzl.swzl.controller;

import com.wzl.swzl.controller.Result.Code;
import com.wzl.swzl.controller.Result.Msg;
import com.wzl.swzl.controller.Result.Result;
import com.wzl.swzl.entity.Wupin;
import com.wzl.swzl.entity.Zhandian;
import com.wzl.swzl.service.WupinService;
import com.wzl.swzl.service.ZhandainService;
import com.wzl.swzl.util.Base64Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.util.Date;
import java.util.UUID;

@Controller
@RequestMapping("/upload")
public class UploadController {

    @Autowired
    private ZhandainService zhandainService;
    @Autowired
    private WupinService wupinService;


    //上传物品信息
    @RequestMapping("/information")
    @ResponseBody
    public Result toImgKey(@RequestBody Wupin wupin) {
        if (wupin.getWupinmingcheng().equals("") || wupin.getTupian().equals("")
                || wupin.getJianshu().equals("") || wupin.getLianxidianhua().equals("")
                || wupin.getUserid().equals("")) {
            return new Result(Code.POST_ERR, Msg.ADD_ERR);
        }
        String base64Str = wupin.getTupian();
        base64Str = base64Str.substring(base64Str.indexOf(",") + 1);
        UUID uid = UUID.randomUUID();
        String pictureId = uid.toString().replace("-", "");
        String imgPath = "D:\\project\\swzl\\src\\main\\webapp\\upload\\" + pictureId + ".jpg";
//        //获取当前class文件路径
//        String path = UploadController.class.getResource("").toString();
        String zhanDianName = wupin.getZhandianmingcheng().trim();
        Zhandian dbZhanDian = null;
        if (!zhanDianName.equals("")) {
            dbZhanDian = zhandainService.findByZhanDianName(zhanDianName);
        }
        if (dbZhanDian == null) {
            return new Result(Code.POST_ERR, Msg.ADD_ERR);
        }
        //保存图片成功再入库
        boolean flag = false;
        if (!base64Str.equals("")) {
            flag = Base64Utils.generateImage(base64Str, imgPath);
        }
        if (!flag) {
            return new Result(Code.POST_ERR, Msg.ADD_ERR);
        }
        String tupian = imgPath.substring(imgPath.indexOf("\\upload"));
        tupian = tupian.replace("\\", "/");
        String bianhao = dbZhanDian.getBianhao();
        String fuzeren = dbZhanDian.getFuzeren();
        String dizhi = dbZhanDian.getDizhi();
        Date addtime = new Date();
        wupin.setBianhao(bianhao);
        wupin.setFuzeren(fuzeren);
        wupin.setDizhi(dizhi);
        wupin.setAddtime(addtime);
        wupin.setTupian(tupian);
        wupin.setShenhejieguo("审核中");
        if (wupin.getZhuangtai().equals("未领取")) {
            if (wupin.getJiandaodizhi().equals("")) {
                return new Result(Code.POST_ERR, Msg.ADD_ERR);
            }
            wupinService.addZLSH(wupin);
        } else {
            if (wupin.getShiqudizhi().equals("") || wupin.getShiquren().equals("")) {
                return new Result(Code.POST_ERR, Msg.ADD_ERR);
            }
            wupinService.addXWSH(wupin);
        }

//        List<Wupin> dbzlsh = wupinService.findZLSH();
        return new Result(Code.POST_OK, Msg.ADD_OK);
    }






}
