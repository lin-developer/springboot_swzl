package com.wzl.swzl.controller.interceptors;

import com.wzl.swzl.entity.User;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        Object loginUser = null;
        try {
            loginUser = request.getSession().getAttribute("loginUser");
            if (null != loginUser) {
                if (loginUser instanceof User) {
                    User user = (User) loginUser;
                    if (!StringUtils.isEmpty(user.getUsername())) {
                        //有登录用户的用户名信息就放行
                        return true;
                    }
                }
            }
            //没有登录凭证
            response.sendRedirect("/swzl/login");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


}
