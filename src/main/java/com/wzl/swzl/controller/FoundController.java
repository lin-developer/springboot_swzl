package com.wzl.swzl.controller;

import com.wzl.swzl.controller.Result.Code;
import com.wzl.swzl.controller.Result.Msg;
import com.wzl.swzl.controller.Result.Result;
import com.wzl.swzl.entity.Page;
import com.wzl.swzl.entity.Wupin;
import com.wzl.swzl.entity.Zhandian;
import com.wzl.swzl.service.WupinService;
import com.wzl.swzl.service.ZhandainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/found")
public class FoundController {
    @Autowired
    private WupinService wupinService;
    @Autowired
    private ZhandainService zhandainService;

    public String wupinmingcheng = "";

    //寻物列表
    @GetMapping("/list/{currPage}")
    @ResponseBody//返回json数据到前端
    public Result list(@PathVariable int currPage) {
        //当前页数
        int pageCount = currPage;
        //总记录数
        Integer totalCount = wupinService.findXunWuTotal();
        //重试次数
        int count = 2;
        while (totalCount == 0 && count > 0) {
            totalCount = wupinService.findXunWuTotal();
            count--;
        }
        //总页数
        Integer totalPage = totalCount % 10 == 0 ? totalCount / 10 : totalCount / 10 + 1;
        //如果记录数为0或请求页面大于总页数或为负数
        if (pageCount > totalPage || pageCount < 0 || totalCount == 0) {
            return new Result(Code.GET_ERR, Msg.QUERY_ERR);
        } else {//如果数据正常则写成json格式返回前端
            //每页显示数,10条
            Integer pageSize = 10;
            // 计算当前查询页数
            Integer startIndex = (pageCount - 1) * pageSize;
            List<Wupin> wuPinEntitys = wupinService.findXunWuList(startIndex, pageSize);
            //重试次数
            int retry = 2;
            while (wuPinEntitys.size() == 0 && retry > 0) {
                wuPinEntitys = wupinService.findXunWuList(startIndex, pageSize);
                retry--;
            }

            if (wuPinEntitys.size() > 0) {
                Map<String,Object> map = new HashMap<>();
                map.put("status","true");
                map.put("totalPage",totalPage);
                map.put("totalCount",totalCount);
                map.put("currPage",pageCount);
                map.put("data",wuPinEntitys);
                return new Result(Code.GET_OK,map,Msg.QUERY_OK);
            } else {
                return new Result(Code.GET_ERR, Msg.QUERY_ERR);
            }
        }
    }


    //寻物页面搜索信息处理
    @PostMapping("/search")
    public String search(Wupin Wupin, Model model, HttpSession session) {
        wupinmingcheng = Wupin.getWupinmingcheng();
        if (wupinmingcheng.equals("")) {
//            model.addAttribute("errorMsg", "请输入关键词!");
            Object obj = session.getAttribute("dbWupin");
            if (null != obj) {
                session.removeAttribute("dbWupin");
            }
            return "found2";
        }
        wupinmingcheng = "%" + wupinmingcheng + "%";
        List<Wupin> dbWupin = wupinService.findXunWuByWuPinName(wupinmingcheng);
        if (dbWupin.size() > 0) {
            return "redirect:/found/resSearch";
        } else {
//            model.addAttribute("errorMsg", "该物品不存在!");
            Object obj = session.getAttribute("dbWupin");
            if (null != obj) {
                session.removeAttribute("dbWupin");
            }
            return "found2";
        }
    }

    //返回搜索结果
    @GetMapping("/resSearch")
    public String resSearch(HttpSession session) {
        List<Wupin> dbWupin = wupinService.findXunWuByWuPinName(wupinmingcheng);
        session.setAttribute("dbWupin", dbWupin);
        return "found2";
    }

    //寻物页面详情页
    @GetMapping("/content")
    public String content(String id, Map<String, Object> map) {
        Wupin dbWupin = wupinService.findXunWuById(id);
        String zhanDianName = dbWupin.getZhandianmingcheng();
        Zhandian dbZhanDian = null;
        if (!zhanDianName.equals("")) {
            dbZhanDian = zhandainService.findByZhanDianName(zhanDianName);
        }
        dbWupin.setLianxidianhua(dbZhanDian.getLianxidianhua());
        map.put("dbWupin", dbWupin);
        return "foundContent";
    }


    //个人中心寻物审核中列表
    @PostMapping("/userXWSH")
    @ResponseBody//返回json数据到前端
    public Result userXunWu(@RequestBody Wupin wupin) {

        String userid = wupin.getUserid();
        userid = userid.replace(" ", "");
        if ("".equals(userid) || userid.length() == 0) {
            return new Result(Code.POST_ERR, Msg.QUERY_ERR);
        } else {
            List<Wupin> wuPinEntitys = wupinService.findXWSHByUserId(wupin.getUserid());
            if (wuPinEntitys.size() > 0) {
                Map<String,Object> map = new HashMap<>();
                map.put("status","true");
                map.put("data",wuPinEntitys);
                return new Result(Code.POST_OK,map,Msg.QUERY_OK);
            } else {
                return new Result(Code.POST_ERR, Msg.QUERY_ERR);
            }
        }
    }

    //个人中心寻物审核未通过列表
    @PostMapping("/userXWJJ")
    @ResponseBody//返回json数据到前端
    public Result userXWJJ(@RequestBody Wupin wupin) {
        String userid = wupin.getUserid();
        userid = userid.replace(" ", "");
        if ("".equals(userid) || userid.length() == 0) {
            return new Result(Code.POST_ERR, Msg.QUERY_ERR);
        } else {
            List<Wupin> wuPinEntitys = wupinService.findXWSHByUserIdJJ(wupin.getUserid());
            if (wuPinEntitys.size() > 0) {
                Map<String,Object> map = new HashMap<>();
                map.put("status","true");
                map.put("data",wuPinEntitys);
                return new Result(Code.POST_OK,map,Msg.QUERY_OK);
            } else {
                return new Result(Code.POST_ERR, Msg.QUERY_ERR);
            }
        }
    }


    //个人中心寻物审核详情页
    @GetMapping("/XWSHContent")
    public String XWSHContent(String id, Map<String, Object> map) {
        if ("".equals(id) || id.length() == 0) {
            return "false";
        }
        Wupin dbWupin = wupinService.findXWSHById(id);
        map.put("dbWupin", dbWupin);
        return "userXWContent";
    }


    //个人中心寻物审核删除
    @GetMapping("/XWSHDelete")
    public String XWSHDelete(String id) {
        if ("".equals(id) || id.length() == 0) {
            return "false";
        }
        wupinService.deleteXWSHById(id);
        return "userXunWu";
    }

    //个人中心寻物公示中列表
    @RequestMapping("/userXWXZ")
    @ResponseBody//返回json数据到前端
    public Result userXWXZ(@RequestBody Wupin wupin) {

        String userid = wupin.getUserid();
        userid = userid.replace(" ", "");
        if ("".equals(userid) || userid.length() == 0) {
            return new Result(Code.POST_ERR, Msg.QUERY_ERR);
        } else {
            List<Wupin> wuPinEntitys = wupinService.findXWByUserId(wupin.getUserid());
            if (wuPinEntitys.size() > 0) {
                Map<String,Object> map = new HashMap<>();
                map.put("status","true");
                map.put("data",wuPinEntitys);
                return new Result(Code.POST_OK,map,Msg.QUERY_OK);
            } else {
                return new Result(Code.POST_ERR, Msg.QUERY_ERR);
            }
        }

    }

    //个人中心寻物已完成列表
    @RequestMapping("/userXWXZWC")
    @ResponseBody//返回json数据到前端
    public Result userXWXZWC(@RequestBody Wupin wupin) {
        String userid = wupin.getUserid();
        userid = userid.replace(" ", "");
        if ("".equals(userid) || userid.length() == 0) {
            return new Result(Code.POST_ERR, Msg.QUERY_ERR);
        } else {
            List<Wupin> wuPinEntitys = wupinService.findXWByUserIdWC(wupin.getUserid());
            if (wuPinEntitys.size() > 0) {
                Map<String,Object> map = new HashMap<>();
                map.put("status","true");
                map.put("data",wuPinEntitys);
                return new Result(Code.POST_OK,map,Msg.QUERY_OK);
            } else {
                return new Result(Code.POST_ERR, Msg.QUERY_ERR);
            }
        }
    }

    //个人中心寻物详情页
    @GetMapping("/XWContent")
    public String XWContent(String id, Map<String, Object> map) {
        if ("".equals(id) || id.length() == 0) {
            return "false";
        }
        Wupin dbWupin = wupinService.findXunWuById(id);
        map.put("dbWupin", dbWupin);
        return "userXWContent";
    }

    //个人中心寻物删除
    @GetMapping("/XWDelete")
    public String XWDelete(String id) {
        if ("".equals(id) || id.length() == 0) {
            return "false";
        }
        wupinService.deleteXWById(id);
        return "userXunWu";
    }

    //管理员寻物审核列表
    @RequestMapping("/HTXWSH")
    @ResponseBody//返回json数据到前端
    public Result HTXWSH() {
        List<Wupin> wuPinEntitys = wupinService.findAllXWSH();
        if (wuPinEntitys.size() > 0) {
            Map<String,Object> map = new HashMap<>();
            map.put("status","true");
            map.put("data",wuPinEntitys);
            return new Result(Code.GET_OK,map,Msg.QUERY_OK);
        } else {
            return new Result(Code.GET_ERR, Msg.QUERY_ERR);
        }
    }

    //管理员查看寻物审核详情页
    @GetMapping("/HTXWSHContent")
    public String HTXWSHContent(String id, Map<String, Object> map) {
        if ("".equals(id) || id.length() == 0) {
            return "false";
        }
        Wupin dbWupin = wupinService.findXWSHById(id);
        map.put("dbWupin", dbWupin);
        return "admin/houTaiXWContent";
    }

    //管理员寻物审核同意
    @GetMapping("/HTXWSHTYUpdate")
    public String HTXWSHTYUpdate(String id) {
        if ("".equals(id) || id.length() == 0) {
            return "false";
        }
        wupinService.updateXWSH(id);
        Wupin dbWupin = wupinService.findXWSHById(id);
        wupinService.addXW(dbWupin);
        return "admin/houTaiIndex";
    }

    //管理员寻物审核拒绝
    @GetMapping("/HTXWSHJJUpdate")
    public String HTXWSHJJUpdate(String id) {
        if ("".equals(id) || id.length() == 0) {
            return "false";
        }
        wupinService.updateJJXWSH(id);
        return "admin/houTaiIndex";
    }

    //管理员寻物公告公示中列表
    @RequestMapping("/HTXWXZ")
    @ResponseBody//返回json数据到前端
    public Result HTXWXZ() {
        List<Wupin> wuPinEntitys = wupinService.findAllXunWu();
        if (wuPinEntitys.size() > 0) {
            Map<String,Object> map = new HashMap<>();
            map.put("status","true");
            map.put("data",wuPinEntitys);
            return new Result(Code.GET_OK,map,Msg.QUERY_OK);
        } else {
            return new Result(Code.POST_ERR, Msg.QUERY_ERR);
        }
    }

    //管理员查看寻物公告公示中的详情页
    @GetMapping("/HTXWXZContent")
    public String HTXWXZContent(String id, Map<String, Object> map) {
        if ("".equals(id) || id.length() == 0) {
            return "false";
        }
        Wupin dbWupin = wupinService.findXunWuById(id);
        map.put("dbWupin", dbWupin);
        return "admin/houTaiXWContent";
    }

    //管理员提交完成寻物公示中的公告
    @GetMapping("/HTXWXZWCUpdate")
    public String HTXWXZWCUpdate(String id) {
        if ("".equals(id) || id.length() == 0) {
            return "false";
        }
        wupinService.updateXWXZ(id);
        return "admin/houTaiIndex";
    }

    //管理员删除寻物公示公告
    @GetMapping("/HTXWXZDelete")
    public String HTXWXZDelete(String id) {
        if ("".equals(id) || id.length() == 0) {
            return "false";
        }
        wupinService.deleteXWById(id);
        return "admin/houTaiIndex";
    }

    //管理员寻物公告已完成列表
    @RequestMapping("/HTXWWC")
    @ResponseBody//返回json数据到前端
    public Result HTXWWC() {
        List<Wupin> wuPinEntitys = wupinService.findAllXWWC();
        if (wuPinEntitys.size() > 0) {
            Map<String,Object> map = new HashMap<>();
            map.put("status","true");
            map.put("data",wuPinEntitys);
            return new Result(Code.GET_OK,map,Msg.QUERY_OK);
        } else {
            return new Result(Code.POST_ERR, Msg.QUERY_ERR);
        }

    }

}
