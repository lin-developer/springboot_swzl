package com.wzl.swzl.controller.advice;

import com.wzl.swzl.controller.Result.Code;
import com.wzl.swzl.controller.Result.Result;
import com.wzl.swzl.exception.BusinessException;
import com.wzl.swzl.exception.SystemException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//异常管理
@RestControllerAdvice
public class ProjectExceptionAdvice {

    //处理系统异常
    @ExceptionHandler(SystemException.class)
    public Result doSystemException(SystemException ex) {
        //记录日志
        //发送消息给运维
        //发送邮件给开发人员，ex对象发给开发人员
        return new Result(ex.getCode(), null, ex.getMessage());
    }


    //处理业务异常
    @ExceptionHandler(BusinessException.class)
    public Result doBusinessException(BusinessException ex) {
        return new Result(ex.getCode(), null, ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public Result doException(Exception ex) {
        //记录日志
        //发送消息给运维
        //发送邮件给开发人员，ex对象发给开发人员
        return new Result(Code.SYSTEM_UNKNOW_ERR, null, "系统繁忙，请联系管理员！");
    }
}
