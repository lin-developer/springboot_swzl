package com.wzl.swzl.controller;

import com.wzl.swzl.controller.Result.Code;
import com.wzl.swzl.controller.Result.Msg;
import com.wzl.swzl.controller.Result.Result;
import com.wzl.swzl.entity.Notice;
import com.wzl.swzl.entity.User;
import com.wzl.swzl.entity.Wupin;
import com.wzl.swzl.service.NoticeService;
import com.wzl.swzl.service.UserService;
import com.wzl.swzl.service.WupinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/swzl")
public class IndexController {
    @Autowired
    private WupinService wupinService;
    @Autowired
    private NoticeService noticeService;
    @Autowired
    private UserService userService;

    public String wupinmingcheng = "";

    //跳转首页
    @GetMapping("/index")
    public String index(Model model) {
        return "index";
    }

    //跳转寻物list页面
    @GetMapping("/found")
    public String found(Model model) {
        return "found";
    }

    //跳转招领list页面
    @GetMapping("/lost")
    public String lost(Model model) {
        return "lost";
    }

    //跳转留言墙页面
    @GetMapping("/lyq")
    public String liuYan(Model model) {
        return "liuyanqiang";
    }

    //跳转到站点list
    @GetMapping("/zdList")
    public String zdList(Model model) {
        return "zhandian";
    }

    //跳转到平台指南
    @GetMapping("/ptzn")
    public String pTZN(Model model) {
        return "pingtaizhinan";
    }

    //跳转到平台指南
    @GetMapping("/login")
    public String login(Model model) {
        return "login";
    }


    //打开主页自动获取公告
    @RequestMapping("/notice")
    @ResponseBody//返回json数据到前端
    public Result ajaxIndexGG() {
        //显示最新三条全站公告
        List<Notice> noticeEntitys = noticeService.findNotice(0, 3);
        Integer code = noticeEntitys != null ? Code.GET_OK : Code.GET_ERR;
        String msg = noticeEntitys != null ? Msg.QUERY_OK : Msg.QUERY_ERR;
        return new Result(code, noticeEntitys, msg);
    }


    //打开主页自动获取招领信息
    @RequestMapping("/zhaoling")
    @ResponseBody//返回json数据到前端
    public Result ajaxIndexZL() {
        List<Wupin> wuPinEntitys = wupinService.findZhaoLingList(0, 3);
        Integer code = wuPinEntitys != null ? Code.GET_OK : Code.GET_ERR;
        String msg = wuPinEntitys != null ? Msg.QUERY_OK : Msg.QUERY_ERR;
        return new Result(code, wuPinEntitys, msg);
    }


    //打开主页自动获取寻物信息
    @RequestMapping("/xunwu")
    @ResponseBody//返回json数据到前端
    public Result ajaxIndexXW() {
        List<Wupin> wuPinEntitys = wupinService.findXunWuList(0, 3);
        Integer code = wuPinEntitys != null ? Code.GET_OK : Code.GET_ERR;
        String msg = wuPinEntitys != null ? Msg.QUERY_OK : Msg.QUERY_ERR;
        return new Result(code, wuPinEntitys, msg);
    }

    //显示公告
    @GetMapping("/ggContent")
    public String ggContent(String id, Map map) {
        Notice dbNotice = noticeService.findNoticeById(id);
        map.put("dbNotice", dbNotice);
        return "ggContent";
    }

    //主页搜索
    @PostMapping("/search")
    public String search(Wupin Wupin, Model model, HttpSession session) {
        wupinmingcheng = Wupin.getWupinmingcheng();
        if (wupinmingcheng.equals("")) {
//            model.addAttribute("errorMsg", "请输入关键词!");
            Object obj = session.getAttribute("dbWupin");
            if (null != obj) {
                session.removeAttribute("dbWupin");
            }
            return "/findall";
        }
        wupinmingcheng = "%" + wupinmingcheng + "%";
        List<Wupin> dbWupin = wupinService.findXunWuByWuPinName(wupinmingcheng);
        List<Wupin> dbWupin2 = wupinService.findZhaoLingByWuPinName(wupinmingcheng);
        if (dbWupin.size() > 0 || dbWupin2.size() > 0) {
            return "redirect:/swzl/resSearch";
        } else {
//            model.addAttribute("errorMsg", "该物品不存在!");
            Object obj = session.getAttribute("dbWupin");
            if (null != obj) {
                session.removeAttribute("dbWupin");
            }
            return "/findall";
        }
    }

    //显示主页搜索结果
    @GetMapping("/resSearch")
    public String resSearch(HttpSession session) {
        List<Wupin> dbWupin = wupinService.findXunWuByWuPinName(wupinmingcheng);
        List<Wupin> dbWupin2 = wupinService.findZhaoLingByWuPinName(wupinmingcheng);
        dbWupin.addAll(dbWupin2);
        session.setAttribute("dbWupin", dbWupin);
        return "/findall";
    }

    //后台获取公告列表
    @RequestMapping("/HTGGList")
    @ResponseBody//返回json数据到前端
    public Result HTGGList() {
        List<Notice> noticeEntitys = noticeService.findAllNotice();
        if (noticeEntitys.size() > 0) {
            Map<String,Object> map = new HashMap<>();
            map.put("status","true");
            map.put("data",noticeEntitys);
            return new Result(Code.GET_OK,map,Msg.QUERY_OK);
        } else {
            return new Result(Code.GET_ERR, Msg.QUERY_ERR);
        }
    }

    //后台显示公告详情
    @GetMapping("/HTZDContent")
    public String HTZDContent(String id, Map map) {
        Notice dbNotice = noticeService.findNoticeById(id);
        map.put("dbNotice", dbNotice);
        return "admin/houTaiggContent";
    }

    //后台删除公告
    @GetMapping("/HTZDDelete")
    public String HTZDDelete(String id) {
        if ("".equals(id) || id.length() == 0) {
            return "false";
        }
        noticeService.deleteGGById(id);
        return "admin/houTaiGongGao";
    }

    //上传公告
    @RequestMapping("/information")
    @ResponseBody
    public Result liuYanAdd(@RequestBody Notice notice) {
        Integer userid = notice.getG_userid();
        String g_username = notice.getG_username();
        if (StringUtils.isEmpty(userid) || StringUtils.isEmpty(g_username) ||
                StringUtils.isEmpty(notice.getG_title()) ||
                StringUtils.isEmpty(notice.getG_content())) {
            return new Result(Code.POST_ERR, Msg.ADD_ERR);
        }
        User dbuser = userService.findByUserId(userid);
        Date addTime = new Date();
        notice.setG_addTime(addTime);
        notice.setG_authority(dbuser.getAuthority());
        boolean flag = noticeService.addGG(notice);
        Integer code = flag ? Code.GET_OK : Code.GET_ERR;
        String msg = flag ? Msg.QUERY_OK : Msg.QUERY_ERR;
        return new Result(code,msg);
    }
}
