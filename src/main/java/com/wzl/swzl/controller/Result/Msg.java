package com.wzl.swzl.controller.Result;

public class Msg {
    public static final String LONGIN_OK = "登录成功！";
    public static final String QUERY_OK = "数据查询成功！";
    public static final String ADD_OK = "添加成功！";
    public static final String UPDATE_OK = "修改成功！";
    public static final String DELETE_OK = "删除成功！";
    public static final String LOGOUT_OK = "注销成功！";


    public static final String LONGIN_ERR = "账号或密码错误！";
    public static final String QUERY_ERR = "数据查询失败，请重试！";
    public static final String ADD_ERR = "添加失败！请重试！";
    public static final String UPDATE_ERR = "修改失败！";
    public static final String DELETE_ERR = "删除失败！";
    public static final String LOGOUT_ERR = "注销失败！";


    public static final String USERNAME_NULL = "用户名为空，请重新输入！";
    public static final String USERNAME_REPEAT = "用户名重复！请重新输入！";
}
